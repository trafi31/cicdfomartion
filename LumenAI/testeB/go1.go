package main
 
import (
    "io"
    "net/http"
    "encoding/json"
)
type Post struct {
    Title  string `json:"Title"`
    Author string `json:"Author"`
    Text   string `json:"Text"`
}
func welcome(w http.ResponseWriter, r *http.Request) {
    io.WriteString(w, "Welcome!")
}
func PostsHandler(w http.ResponseWriter, r *http.Request) {
 
    posts := []Post{
        Post{"Post one", "John", "This is first post."},
        Post{"Post two", "Jane", "This is second post."},
        Post{"Post three", "John", "This is another post."},
    }
 
    json.NewEncoder(w).Encode(posts)
}
func main() {
 
    mux := http.NewServeMux()
 
    mux.HandleFunc("/welcome", welcome)
    mux.HandleFunc("/posts", PostsHandler)
    http.ListenAndServe(":5050", mux)
}