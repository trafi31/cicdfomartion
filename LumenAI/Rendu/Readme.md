# description
Ceci est un projet qui met en facteur golang et javascript.
Grâce a kubernetes.

## Demander
Le but de l'exercice est de créer un service déployé kubernetes.
1° pod front end:
- une page html composée de deux champs de formulaire. Au click sur le bouton "Envoyer" un appel javascript est exécuté au endpoint /multiply afin de récupérer et afficher le résultat de la multiplication des deux champs de formulaire.
2° pod back end:
- Une api qui suivra la spécification décrite dans le fichier apidoc.html
le fichier "technical.png" en référence précise le besoin.
Le déploiement de cette architecture devra être automatisé au maximum.

# Prérequis
- minikube v1.14.2
- docker 19.03.13
- kubernetes v1.19.2

## Commentaire
- Je fais tout sur WSL2
- Le lancement de minikube ce fait avec le driver docker.
- Je n'ai pas réussi à faire l'ingress pour que les deux application communique entre eux.
- Je voulais mettre ingress pour faciliter l'adressage url.
- Le lancement des services se font avec des images docker en local.

# Démmarage
## minikube
```bash
minikube start --driver=docker
minikube docker-env
eval $(minikube -p minikube docker-env)
```
## images docker local
```bash
docker build -t antoine/appgo back/.
docker build -t antoine/appjs front/.
```
## run kubernetes
```bash
kubectl apply -f MyApp.yml
```
## tester les services
```bash
minikube service myapp-back --url
minikube service myapp-front --url
```
pour la partie backend. Vous pouvez rajouter vos numeros 
<-url-> /multiply/4/7  => http://127.0.0.1:44847/multiply/4/7
## stop kubernetes
```bash
kubectl delete -f MyApp.yml
```
## voire le dashboard
```bash
minikube dashboard
```
## CMD ingress non Réussi
```bash
minikube ssh
docker pull quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.33.0
minikube addons enable ingress
kubectl apply -f ingress.yml
kubectl get ingress
```
